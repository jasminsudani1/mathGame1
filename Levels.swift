//
//  Levels.swift
//  GAMEpn
//
//  Created by jasmin on 01/04/2023.
//

import UIKit
var questionsCount = 0
class Levels: UIViewController {
    
    @IBOutlet weak var myEsay: UIButton!
    @IBOutlet weak var myMedium: UIButton!
    @IBOutlet weak var myHard: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myEsay.layer.cornerRadius = 10
        myEsay.layer.borderWidth = 3.5
        myMedium.layer.cornerRadius = 10
        myMedium.layer.borderWidth = 3.5
        myHard.layer.cornerRadius = 10
        myHard.layer.borderWidth = 3.5
    }
    
    @IBAction func onClickEasy(_ sender: UIButton) {
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Questions")
        self.navigationController?.pushViewController(x, animated: true)
        questionsCount = 2
    }

    @IBAction func onClickMedium(_ sender: UIButton) {
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Questions")
        self.navigationController?.pushViewController(x, animated: true)
        questionsCount = 3
    }
    
    @IBAction func onClickHard(_ sender: UIButton) {
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Questions")
        self.navigationController?.pushViewController(x, animated: true)
        questionsCount = 5
    }
}

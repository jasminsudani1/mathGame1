//
//  ViewController.swift
//  GAMEpn
//
//  Created by jasmin on 01/04/2023.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myPlay: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        myPlay.layer.cornerRadius = 10
        myPlay.layer.borderWidth = 3.5
        
    }

    @IBAction func onClickPlay(_ sender: Any) {
        let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Levels")
        self.navigationController?.pushViewController(x, animated: true)
        
    }
    
}


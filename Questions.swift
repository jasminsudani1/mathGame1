
//
//  Questions.swift
//  GAMEpn
//
//  Created by jasmin on 01/04/2023.
//

import UIKit

struct GameQuestionsAndAnswer {
    var questions = String()
    var options = [String]()
    var answer = String()
}



let o1 = GameQuestionsAndAnswer(questions: "10 + 20 (__) 20 = 10", options: ["-", "+" ,"*" ,"/"], answer: "-")
let o2 = GameQuestionsAndAnswer(questions: "10 + 20 (__) 3 = 90", options: ["*", "/" ,"-" ,"+"], answer: "*")
let o3 = GameQuestionsAndAnswer(questions: "10 * 20 (__) 10 = 20", options: ["/", "+" ,"*" ,"-"], answer: "/")
let o4 = GameQuestionsAndAnswer(questions: "10 (__) 30 - 10 = 30", options: ["*", "+" ,"-" ,"/"], answer: "+")
let o5 = GameQuestionsAndAnswer(questions: "11 (__) 8 + 3 = 91", options: ["/", "-" ,"*" ,"="], answer: "*")


class Questions: UIViewController {
    
    var object = [o1,o2,o3,o4,o5]
    var count = 0
    var exchange = [GameQuestionsAndAnswer]()
    var score = 0
    
    @IBOutlet var optionsAll: [UIButton]!
    @IBOutlet weak var myLiveScore: UILabel!
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var myNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myLabel.layer.cornerRadius = 15
        myLabel.layer.borderWidth = 5
        
        optionsAll .forEach { i in
            i.layer.cornerRadius = 8
        }
        myNext.layer.cornerRadius = 10
        myNext.isEnabled = false
        myLiveScore.layer.cornerRadius = 10
        myLiveScore.text = "Score = " + "0"
        exchange = object.shuffled()
        myLabel.text = exchange[count].questions
        call(whichObj: exchange[count])
    }

    func call (whichObj : GameQuestionsAndAnswer) {
        var countT = 0
        optionsAll .forEach { i in
            i.setTitle(whichObj.options[countT], for: .normal)
        }
        countT += 1
    }
       
    func setAllEnable (bool : Bool) {
        optionsAll .forEach { i in
            i.isEnabled = bool
        }
        myNext.isEnabled = false

    }
    func setAllDisable (bool : Bool) {
        optionsAll .forEach { i in
            i.isEnabled = bool
        }
        myNext.isEnabled = true

    }
    func setWhiteColourWithBorder () {
        optionsAll .forEach { i in
            i.backgroundColor = .white
            i.layer.borderWidth = 0
        }
    }
    
    @IBAction func onClickNext(_ sender: UIButton) {
        count += 1
        if count != questionsCount {
            print(questionsCount)
            myLabel.text = exchange[count].questions
            myLabel.layer.borderWidth = 5
            call(whichObj: exchange[count])
        }
        else {
            let alt = UIAlertController(title: "\(score)", message: "Is Your Score", preferredStyle: .alert)
            let x2 = UIAlertAction(title: "RESTART", style: .default) { _ in
                self.navigationController?.popViewController(animated: true)
            }
            alt.addAction(x2)
            self.present(alt, animated: true)
        }
        setWhiteColourWithBorder()
        setAllEnable(bool: true)
    }

    @IBAction func onClickAnyOption(_ send : UIButton) {
        if send.currentTitle! == exchange[count].answer {
            score+=1
            myLiveScore.text = "Score = " + String(score)
            send.backgroundColor = .green
            send.layer.borderWidth = 2.5
        } else {
            send.backgroundColor = .red
            for i in optionsAll {
                if i.titleLabel?.text == exchange[count].answer {
                    i.backgroundColor = .green
                    i.layer.borderWidth = 3
                }
            }
        }
        setAllDisable(bool : false)
    }
}
